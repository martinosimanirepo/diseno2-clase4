﻿using Ecommerce.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace Ecommerce.WebApi.Controllers
{
    public class UsersController : ApiController
    {
        User[] users = new User[] { 
            new User() { Name = "Alejandro", LastName = "Tocar", Email ="aletocar@gmail.com", UserId = 1 },
            new User() { Name = "Ignacio", LastName = "Fonseca", Email ="ifonseca@montevideo.com.uy", UserId = 2 },
            new User() { Name = "Juan Ignacio", LastName = "Galán", Email ="juanigalan91@gmail.com", UserId = 3 }
        };
        public IEnumerable<User> GetAllUsers()
        {
            return users;
        }

        public IHttpActionResult GetUser(int id)
        {
            var user = users.FirstOrDefault((u) => u.UserId == id);
            if (user == null)
            {
                return NotFound();
            }
            return Ok(user);
        }
    }
}